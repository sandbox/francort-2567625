<?php
/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */


/**
 * This class calculates working days within data ranges.
 */
class WorkingDays {
  /**
   * Holidays fixed for each year. No exception.
   *
   * @var array
   */
  protected $freeDaysAll;


  /**
   * Variable holiday. Different from one year to the other.
   *
   * @var array
   */
  protected $freeDaysException;

  /**
   * Days of the week considered free. No exception.
   *
   * From 1 to 7. 1 is monday and 7 is sunday.
   *
   * @var array
   */
  protected $freeWeekDays;



  /* Constructor */
  public function __construct() {
    $this->freeDaysAll       = array();
    $this->freeDaysException = array();
    $this->freeWeekDays      = array();
  }


  /**
   * Sets the days of the week in which there is no work.
   *
   * @param array $weekDays
   *  - each value must be in the range [1, 7]. 1 for monday, 7 for sunday.
   *
   * @return \WorkingDays
   * @throws InvalidArgumentException
   */
  public function setFreeWeekDays($weekDays) {
    foreach ($weekDays as $weekDay) {
      if (!is_numeric($weekDay) || $weekDay > 7 || $weekDay < 1) {
        throw new InvalidArgumentException("Week day must be a number in [1-7] range");
      }
    }

    $this->freeWeekDays = $weekDays;

    return $this;
  }



 /**
  * Gets common FreeDays defined.
  *
  * @return type
  */
  public function getFreeDaysAll() {
    return $this->freeDaysAll;
  }



  /**
   * Adds a holiday to the list.
   *
   * @param integer $day
   *    Between 1 and 31
   * @param integer $month
   *    Between 1 and 12
   * @param mixed $year
   *    if the year is FALSE then is a holiday valid for each year. If is
   *    numeric then is just valid for that number.
   *
   * @return \WorkingDays
   * @throws InvalidArgumentException
   */
  public function addFreeDay($day, $month, $year = FALSE) {
    if (!is_int($day) || !is_int($month)) {
      throw new InvalidArgumentException("Day and month should be integer");
    }
    if ($year !== FALSE && !is_int($year)) {
      throw new InvalidArgumentException("Year should be integer");
    }
    if ($year !== FALSE) {
      $this->freeDaysException[$year][$month][$day] = TRUE;
    }
    else {
      $this->freeDaysAll[$month][$day] = TRUE;
    }

    return $this;
  }



  /**
   * Calculate working days between 2 dates (both included)
   *
   * @param timestamp $startDate
   * @param timestamp $endDate
   *
   * @return int
   *    How many working days
   */
  public function calculateWorkingDays($startDate, $endDate) {
    $workingDays = 0;

    $currentDate = $startDate;
    $secondsInDay = (60*60*24);

    while ($currentDate < $endDate) {

      if (in_array(date("N", $currentDate), $this->freeWeekDays)) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      if (isset($this->freeDaysAll[$currentMonth][$currentDay])) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      if (isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay])) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      $currentDate = $currentDate + $secondsInDay;
      ++$workingDays;
    }

    return $workingDays;
  }



  /**
   * Given a start date and a number of work days, it returns the final date.
   *
   * @param timestamp $startDate
   * @param integer $workingDays
   *
   * @return timestamp
   */
  public function calculateEndDate($startDate, $workingDays) {
    $daysToConsume = $workingDays;

    $currentDate = $startDate;
    $minutesDay = (60*60*24);

    while ($daysToConsume > 0) {

      $currentDate = $currentDate + $minutesDay;
      $isWeekend = in_array(date("N", $currentDate), $this->freeWeekDays);

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      $thisIsFree =
          isset($this->freeDaysAll[$currentMonth][$currentDay]) ||
          isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay]);

      if ($thisIsFree || $isWeekend) {
        continue;
      }
      else {
        --$daysToConsume;
      }
    }

    return $currentDate;
  }


  /**
   * Gives all free days in a month for a specific year.
   *
   * @param int $month
   * @param int $year
   * @return array
   *    - The array values are the free days for that month and year,
   */
  function getMonthFreeDays($month, $year) {
    $freeDays = array();
    $currentDate = mktime(0, 0, 0, $month, 1, $year);
    $currentMonth = $month;

    $secondsDay = (60*60*24);

    $i = 1;
    while ($currentMonth === $month) {
      $isWeekend = in_array(date("N", $currentDate), $this->freeWeekDays);

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      $thisIsFree =
        isset($this->freeDaysAll[$currentMonth][$currentDay]) ||
        isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay]);

      if ($thisIsFree || $isWeekend) {
        $freeDays[] = $i;
      }

      $currentDate += $secondsDay;
      $currentMonth = (int) date("m", $currentDate);

      if ($i++ > 31) break;
    }

    return $freeDays;
  }



  /**
   * Gives all working days in a month for a specific year.
   *
   * @param int $month
   * @param int $year
   * @return array
   *    - The array values are the working days for that month and year,
   */
  function getMonthWorkingDays($month, $year) {
    $workingDays = array();
    $currentDate = mktime(0, 0, 0, $month, 1, $year);
    $currentMonth = $month;

    $secondsDay = (60*60*24);

    $i = 1;
    while ($currentMonth === $month) {
      $isWeekend = in_array(date("N", $currentDate), $this->freeWeekDays);

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      $thisIsFree =
        isset($this->freeDaysAll[$currentMonth][$currentDay]) ||
        isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay]);

      if (!$thisIsFree && !$isWeekend) {
        $workingDays[] = $i;
      }

      $currentDate += $secondsDay;
      $currentMonth = (int) date("m", $currentDate);

      if ($i++ > 31) break;
    }

    return $workingDays;
  }
}