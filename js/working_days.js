/**
 * @file
 * User interface javascript.
 * 
 * Converts an ugly textarea into a nice form.
 *
 */
(function ($) {
  var rebuildDateInfo = function(){
    var info = "";
    var infoArray = [];
    var j = 0;
    $('.free-days-info input').each(function(i, val){
      var type = $(this).attr('type');
      if(type === "text"){
        infoArray[j] = "" + $(this).val();
      }
      else if(type === "checkbox"){
        if($(this).attr('checked') === false){
          infoArray[j] += "-" + new Date().getFullYear();
        }
        ++j;
      }
    });
    
    info = infoArray.join("\n");
    
    return info;
  };
  
  
  
  Drupal.theme.prototype.freeDayTable = function (dates, labels, addLabel) {
    var output = "";

    output += '<table id="ui-free-days">';
    output += '<tr>';
    output += '<th>' + labels.days + '</th>';
    output += '<th>' + labels.check + '</th>';
    output += '<th>' + labels.operations + '</th>';
    output +=  "</tr>";
    for(i in dates){
      var numbers = dates[i].split("-");
      var checked = " checked=checked";
      if(numbers.length === 3){
        checked = "";
      }
      var currentDate = numbers[0] + '-' + numbers[1];
      output += Drupal.theme('freeDayRow', currentDate, i, checked, labels.delete);
    }
    output += '</table>';
    
    output += '<a href="#" id="add-free-day">' + addLabel + '</a>';

    return $(output);
  };
  
  
  
  Drupal.theme.prototype.freeDayRow = function (currentDate, index, checked, sDelete) {
    var output = "";

    output += '<tr class="free-days-info">';
    output += '<td>';
    output += '<input name="date-'+index+'" type="text" class="dates" value="'+ currentDate + '">';
    output +=  "</td><td>";
    output += '<input type="checkbox" name="check-'+index+'" '+ checked +'/>';
    output +=  "</td>"; 
    output +=  "</td><td>";
    output += '<a href="#" class="delete">' + sDelete + '</a>';
    output +=  "</td>";
    output +=  "</tr>";
    
    return output;
  };
  
  
  
  Drupal.behaviors.freeDayBehavior = {
    attach: function (context, settings) {
      $('#free-days', context).once('foo', function () {
        var dates = $('#edit-days').val().split("\n");
        var labels = {
          "days"       : settings.working_days.labels.days,
          "check"      : settings.working_days.labels.check,
          "operations" : settings.working_days.labels.operations,
          "delete"     : settings.working_days.labels.delete
        };
        var $table = Drupal.theme('freeDayTable', dates, labels, settings.working_days.addLabel);
        
        $(this).append($table);
        
        
        $('.delete').click(function(){
          $(this).parent().parent().remove();
        });
        
        if($('div.messages.error').length === 0){
          $('.form-item-days').hide();
        }
        
        $("#edit-save").click(function(){
          var info = rebuildDateInfo();
          $('#edit-days').val(info);
        });
        
        $( ".dates" ).datepicker({ dateFormat: "dd-mm" });
        
        /* Event: Adds a new line */
        $('#add-free-day').click(function(){
          var rows = $('#ui-free-days').find('tr').length;
          var today = new Date();
          var day = today.getDate() + "";
          var month = (today.getMonth() + 1) + "";
          
          if(month.length < 2){
            month = "0" + month;
          }
          if(day.length < 2){
            day = "0" + day;
          }
          
          var currentDate = day + "-" + month;
          
          var output = "";
          var sDelete = settings.working_days.labels.delete;
          output += Drupal.theme('freeDayRow', currentDate, rows, "", sDelete);
          $('#ui-free-days').append(output);
          
          $('#ui-free-days .dates:last').datepicker({ dateFormat: "dd-mm" });
          
          $('.delete').click(function(){
            $(this).parent().parent().remove();
            return false;
          });
          
          var info = rebuildDateInfo();
          $('#edit-days').val(info);
          
          $(".free-days-info .dates, .free-days-info input[type=checkbox]").change(function(){
            var info = rebuildDateInfo();
            $('#edit-days').val(info);
          });
          
          return false;
        });
        
        /* Event: refreshes textarea data if table changes */
        $(".free-days-info .dates, .free-days-info input[type=checkbox]").change(function(){
          var info = rebuildDateInfo();
          $('#edit-days').val(info);
        });
      });
    }
  };

})(jQuery);